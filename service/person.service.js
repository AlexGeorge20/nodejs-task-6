// const fs = require("fs");
const { executeQuery } = require("../database");
//GET ALL
async function getPersons(id){
  let persons= await executeQuery(`SELECT studentName,subjectName,marks FROM student INNER JOIN Marks ON student.student_id=Marks.student_id
  INNER JOIN subjects ON subjects.subject_id=Marks.subject_id WHERE ${id}=student.student_id`);
  console.log("IDgetperon",id);
return persons;
}

//GET id
async function personCheckIdService(id,subjectId){
    let personId;
    console.log("Params",id);
    let subId=await executeQuery(`SELECT * FROM Marks WHERE Marks.subject_id=${subjectId}`)
    console.log("CHECKIDin get",subId[0]);
    if(subId[0].length>0){
    personId=await executeQuery(`SELECT studentName,subjectName,marks FROM student INNER JOIN Marks ON student.student_id=Marks.student_id
    INNER JOIN subjects ON subjects.subject_id=Marks.subject_id WHERE ${id}=student.student_id AND ${subjectId}=subjects.subject_id `);
    console.log("personID",personId[0]);
    
    return personId[0];
    }
    else{
      return "ID NOT FOUND";
    }
   }

//DELETE
async function personDeleteIdService(id,subjectId){
      console.log("Params", id);
    let subId=await executeQuery(`SELECT * FROM Marks WHERE Marks.subject_id=${subjectId}`)
  console.log("CHECKIDin del",subId[0]);
  if(subId[0].length>0){
  let personDeleteId=await executeQuery(`DELETE FROM Marks WHERE Marks.subject_id=${subjectId}`);
  return "DELETED";

  }else{
    return "ID NOT FOUND";
  }
}
//CREATE new
async function personCreateService(id,subjId,marks){
  console.log("Param id, subID & marks",id,subjId,marks);  
  let subjectId=await executeQuery(`SELECT subject_id,marks FROM Marks WHERE Marks.subject_id=${subjId} AND Marks.student_id=${id} `)
console.log("SubjctId",subjectId[0]);
if(subjectId[0].length==0){
  let personCreateName=await executeQuery(`INSERT INTO Marks(marks,subject_id,student_id) values(${marks},${subjId},${id})`);
  return "CREATED";
}else{
  return "ALREADY EXIST";
}
 }

//UPDATE SUB id
async function personUpdateService(id,subjectId,bodyId,marks){
    console.log("bodyMarks",marks);
    let subjId=await executeQuery(`SELECT subject_id FROM Marks WHERE Marks.subject_id=${subjectId}`)
    console.log("SubjctId",subjId[0]);
if(subjId[0].length>0){
  let updateMark= await executeQuery(`UPDATE Marks SET marks=${marks} WHERE student_id=${id} AND subject_id=${subjectId}`)
  console.log("YES",marks,id,subjectId);
  return "UPDATED"
}else{
  console.log("NO");
  return "ID NOT FOUND TO UPDATE"
}
  
}
module.exports={personListService: getPersons, personCheckIdService, personDeleteIdService,personCreateService,personUpdateService}
    