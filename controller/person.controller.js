const Logger = require("nodemon/lib/utils/log");
const { personListService, personCheckIdService, personDeleteIdService, personCreateService, personUpdateService } = require("../service/person.service");

const personListController= async(req,res)=>{
    let id= req.params.id;
    const temp =await personListService(id)
    console.log("READ",temp);
    res.status(201).send(temp[0])
}

const personCheckIdControler=async(req,res)=>{
    let id=req.params.id;
    let subjectId= req.params.subjectId;
    const response= await personCheckIdService(id,subjectId);
    res.send(response)
}
const personDeleteIdControler=async(req,res)=>{  
    const id= req.params.id;
    let subjectId= req.params.subjectId;
    const response=await personDeleteIdService(id,subjectId);
    console.log("DELreSPOnse",response);
    res.send(response)
  
}
const personCreateController=async(req,res)=>{  
   const id=req.params.id;
    const bodyId= req.body.id;
    const subjectId= req.params.subjectId;
    const newMarks=req.body.marks;
    // console.log("REQname",newName);
    const response=await personCreateService(id,subjectId,newMarks);
    console.log("reSPOnse",response);
    res.send(response)
  
}
const personUpdateController=async(req,res)=>{
    const id=req.params.id;
    const subjectId= req.params.subjectId;
    const bodyId= req.body.id;
    const newMarks=req.body.marks;
          
       const response=await personUpdateService(id,subjectId,bodyId,newMarks)
       console.log("UpdateRESponse",response);
       res.send(response)
         
    }

module.exports={personListController,personCheckIdControler,personDeleteIdControler,personCreateController,personUpdateController}
