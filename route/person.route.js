const {Router}= require('express');
const { personListController, personCheckIdControler, personDeleteIdControler, personCreateController, personUpdateController } = require('../controller/person.controller');
const router=Router();
let path="/student"


//LIST
router.get(`${path}/:id/marks/`,personListController)
//CHECK ID
router.get(`${path}/:id/marks/:subjectId`, personCheckIdControler)
//Delete ID
router.delete(`${path}/:id/marks/:subjectId`,personDeleteIdControler)
//CREATE
router.post(`${path}/:id/marks/:subjectId`,personCreateController)

//UPDATE
router.put(`${path}/:id/marks/:subjectId`,personUpdateController)

module.exports= {personRouter : router}